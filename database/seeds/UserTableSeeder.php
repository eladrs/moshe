<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name'=>'jack',
                    'email'=>'jack@jack.com',
                    'password'=>'1234',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name'=>'elad',
                    'email'=>'elad@elad.com',
                    'password'=>'12345',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name'=>'gal',
                    'email'=>'gal@gal.com',
                    'password'=>'123456',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name'=>'ben',
                    'email'=>'ben@ben.com',
                    'password'=>'1234567',
                    'created_at' => date('Y-m-d G:i:s'),
                ],


            ]
            );
    }
}
